#!/bin/sh

set -e

RESIZE_SHAPE=500x500
RESIZED_ART=/tmp/resized-album-art.png
ART_AND_WALLPAPER=/tmp/album-art-wallpaper.png
CURRENT_WALL=~/mine/configs/walls/current.png

albumfile="$(playerctl metadata mpris:artUrl | cut -c8- | sed 's/%20/ /g')" 

if [ ! -r "${albumfile}" ]; then
    feh --bg-fill "${CURRENT_WALL}"
    exit 0
fi

# Resize album art.
convert "${albumfile}" -geometry "${RESIZE_SHAPE}" "${RESIZED_ART}"

# Composite album art onto current wallpaper.
convert \
    "${CURRENT_WALL}" "${RESIZED_ART}" \
    -crop 1920x1080+0+0                               \
    -geometry +20+50 \
    -gravity NorthWest                                \
    -composite "${ART_AND_WALLPAPER}"

# Set new wallpaper.
feh --bg-center "${ART_AND_WALLPAPER}"
