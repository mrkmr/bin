#!/bin/sh

ls -d /sys/class/power_supply/BAT*/ | while read dir; do
    start_t="$(cat ${dir}/charge_start_threshold)"
    stop_t="$(cat ${dir}/charge_stop_threshold)"
    echo "${start_t}% - ${stop_t}%"
done
echo

ls -d /sys/class/power_supply/BAT*/ | while read dir; do
    echo "$(cat ${dir}/status) $(cat ${dir}/capacity)%"
done | column -t  -R 2
